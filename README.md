# Desafio Técnico Backend Amiko Soluções

Implementação de Endpoints [RESTful](https://www.youtube.com/watch?v=tPbK3eOJLXQ) para Gestão de Chamados.

## Objetivo:

Atualmente os hospitais possuem dificuldade em gerenciar os chamados dos pacientes quando necessitam de alguma ajuda, como por exemplo:
- Pedido de ajuda para ir ao banheiro;
- Pedido de remédio para dores;

...

O time que você atua será responsável por implementar o projeto. E você será a pessoa do time que implementará dois endpoints:
- O primeiro é um POST que vai criar o chamado;
- O segundo é um GET que pesquisará todos os chamados cadastrados.

Com base nas informações anterior, siga os requisitos a seguir para a implementação.

## Requisitos:

### Endpoint de Criação de Chamado:

Criar uma chamada ao endpoint:

- path: /calls
- Método: POST

#### Dados do Corpo da Requisição:

```json
{
  "id": "1",
  "name": "estou com dor",
  "hospital": {
    "name": "Hospital Ada Lovelace",
    "roomNumber": 101
  }
}
```

#### Resposta Esperada:

```json
{
  "message": "Chamado criado com sucesso",
}
```
#### Validações

- O sistema deve retornar erro se o número do quarto for **menor ou igual a zero**;

**Exemplos de Retorno de erros:**

```json
{
  "message": "O número do quarto deve ser maior ou igual a 1",
}
```

#### Endpoint de Consulta de Chamados por Quarto:

Criar uma chamada ao endpoint:

- path: /calls
- Método: GET


#### Resposta esperada

```json
{
  "calls": [
    {
      "name": "estou com fome",
    },
    {
      "name": "estou com sede",
    },
    ...
  ]
}
```

## Regras de Implementação:

- Utilizar uma linguagem de programação: JavaScript (Node.js) ou Golang;
- Utilizar um banco de dados que você conheça melhor;
- Documentar o código e as instruções para execução do projeto;
- O código fonte pode ser versionado em um repositório a sua escolha (ex: GitHub, GitLab, Bitbucket) ou enviado por email;


## Critérios de Avaliação:

- Lógica utilizada no desenvolvimento.


## Dicas:

Documentos para auxiliar no desenvolvimento:

- [Criando a primeira api nodejs - medium](https://medium.com/@silascastro15/como-criar-sua-primeira-apirestful-com-nodejs-express-mongodb-parte-2-8f59c726233f)
- [Criando a primeira api nodejs - youtube](https://www.youtube.com/watch?v=WejbsEnvvRQ)

**Boa sorte!**






